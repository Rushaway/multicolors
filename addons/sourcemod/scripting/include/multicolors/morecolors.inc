// MOAR COLORS
// By Dr. McKay, maxime1907
// Inspired by: https://forums.alliedmods.net/showthread.php?t=96831

#if defined _more_colors_included
 #endinput
#endif
#define _more_colors_included

#pragma newdecls optional
#include <regex>

#define MORE_COLORS_VERSION     "2.0.0-MC"
#define MC_MAX_MESSAGE_LENGTH   256
#define MAX_BUFFER_LENGTH       (MC_MAX_MESSAGE_LENGTH * 4)

#define MC_GAME_DODS           0

bool MC_SkipList[MAXPLAYERS+1];
StringMap MC_Trie;
int MC_TeamColors[][] = {
	{
		0xCCCCCC,
		0x4D7942,
		0xFF4040
	}
}; // Multi-dimensional array for games that don't support SayText2. First index is the game index (as defined by the GAME_ defines), second index is team. 0 = spectator, 1 = team1, 2 = team2

static ConVar sm_show_activity;

/**
 * Prints a message to a specific client in the chat area.
 * Supports color tags.
 * 
 * @param client		Client index.
 * @param message		Message (formatting rules).
 * 
 * On error/Errors:		If the client is not connected an error will be thrown.
 */
stock void MC_PrintToChat(int client, const char[] message, any ...) {
	MC_CheckTrie();

	if (client <= 0 || client > MaxClients) {
		ThrowError("Invalid client index %i", client);
	}

	if (!IsClientInGame(client)) {
		ThrowError("Client %i is not in game", client);
	}

	char buffer[MAX_BUFFER_LENGTH];
	char buffer2[MAX_BUFFER_LENGTH];

	SetGlobalTransTarget(client);
	Format(buffer, sizeof(buffer), "\x01%s", message);
	VFormat(buffer2, sizeof(buffer2), buffer, 3);

	MC_ReplaceColorCodes(buffer2);
	MC_SendMessage(client, buffer2);
}

/**
 * Prints a message to all clients in the chat area.
 * Supports color tags.
 * 
 * @param client		Client index.
 * @param message		Message (formatting rules).
 */
stock void MC_PrintToChatAll(const char[] message, any ...) {
	MC_CheckTrie();

	char buffer[MAX_BUFFER_LENGTH], buffer2[MAX_BUFFER_LENGTH];

	for (int i = 1; i <= MaxClients; ++i) {
		if (!IsClientInGame(i) || MC_SkipList[i]) {
			MC_SkipList[i] = false;
			continue;
		}

		SetGlobalTransTarget(i);
		Format(buffer, sizeof(buffer), "\x01%s", message);
		VFormat(buffer2, sizeof(buffer2), buffer, 2);

		MC_ReplaceColorCodes(buffer2);
		MC_SendMessage(i, buffer2);
	}
}

/**
 * Prints a message to a specific client in the chat area.
 * Supports color tags and teamcolor tag.
 * 
 * @param client		Client index.
 * @param author		Author index whose color will be used for teamcolor tag.
 * @param message		Message (formatting rules).
 * 
 * On error/Errors:		If the client or author are not connected an error will be thrown
 */
stock void MC_PrintToChatEx(int client, int author, const char[] message, any ...) {
	MC_CheckTrie();

	if (client <= 0 || client > MaxClients) {
		ThrowError("Invalid client index %i", client);
	}

	if (!IsClientInGame(client)) {
		ThrowError("Client %i is not in game", client);
	}

	if (author <= 0 || author > MaxClients) {
		ThrowError("Invalid client index %i", author);
	}

	if (!IsClientInGame(author)) {
		ThrowError("Client %i is not in game", author);
	}

	char buffer[MAX_BUFFER_LENGTH], buffer2[MAX_BUFFER_LENGTH];
	SetGlobalTransTarget(client);
	Format(buffer, sizeof(buffer), "\x01%s", message);
	VFormat(buffer2, sizeof(buffer2), buffer, 4);
	MC_ReplaceColorCodes(buffer2, author);
	MC_SendMessage(client, buffer2, author);
}

/**
 * Prints a message to all clients in the chat area.
 * Supports color tags and teamcolor tag.
 *
 * @param author	  Author index whose color will be used for teamcolor tag.
 * @param message   Message (formatting rules).
 * 
 * On error/Errors:   If the author is not connected an error will be thrown.
 */
stock void MC_PrintToChatAllEx(int author, const char[] message, any ...) {
	MC_CheckTrie();

	if (author <= 0 || author > MaxClients) {
		ThrowError("Invalid client index %i", author);
	}

	if (!IsClientInGame(author)) {
		ThrowError("Client %i is not in game", author);
	}

	char buffer[MAX_BUFFER_LENGTH];
	char buffer2[MAX_BUFFER_LENGTH];

	for (int i = 1; i <= MaxClients; ++i) {
		if (!IsClientInGame(i) || MC_SkipList[i]) {
			MC_SkipList[i] = false;
			continue;
		}

		SetGlobalTransTarget(i);
		Format(buffer, sizeof(buffer), "\x01%s", message);
		VFormat(buffer2, sizeof(buffer2), buffer, 3);

		MC_ReplaceColorCodes(buffer2, author);
		MC_SendMessage(i, buffer2, author);
	}
}

/**
 *
 * Adds a whitespace for games other than Source2009
 *
 * @param buffer	Buffer that contains the chat message
 * @param size		Size of the buffer
 */
stock void MC_AddWhiteSpace(char[] buffer, int size)
{
	EngineVersion iEngineVersion = GetEngineVersion();
	if (!IsSource2009() && !(iEngineVersion == Engine_Left4Dead) && !(iEngineVersion == Engine_Left4Dead2)) {
		Format(buffer, size, " %s", buffer);
	}
}

/**
 * Sends a SayText2 usermessage
 * 
 * @param client	Client to send usermessage to
 * @param message	Message to send
 */
stock void MC_SendMessage(int client, const char[] message, int author = 0) {
	if (author == 0) {
		author = client;
	}

	char buffer[MC_MAX_MESSAGE_LENGTH];
	strcopy(buffer, sizeof(buffer), message);

	MC_AddWhiteSpace(buffer, sizeof(buffer));

	UserMsg index = GetUserMessageId("SayText2");
	if (index == INVALID_MESSAGE_ID) {
		if (GetEngineVersion() == Engine_DODS) {
			int team = GetClientTeam(author);
			if (team == 0) {
				ReplaceString(buffer, sizeof(buffer), "\x03", "\x04", false); // Unassigned gets green
			}
			else {
				char temp[16];
				Format(temp, sizeof(temp), "\x07%06X", MC_TeamColors[MC_GAME_DODS][team - 1]);
				ReplaceString(buffer, sizeof(buffer), "\x03", temp, false);
			}
		}

		PrintToChat(client, "%s", buffer);
		return;
	}

	Handle buf = StartMessageOne("SayText2", client, USERMSG_RELIABLE|USERMSG_BLOCKHOOKS);
	if (GetFeatureStatus(FeatureType_Native, "GetUserMessageType") == FeatureStatus_Available && GetUserMessageType() == UM_Protobuf) {
		Protobuf pb = UserMessageToProtobuf(buf);
		pb.SetInt("ent_idx", author);
		pb.SetBool("chat", true);
		pb.SetString("msg_name", buffer);
		pb.AddString("params", "");
		pb.AddString("params", "");
		pb.AddString("params", "");
		pb.AddString("params", "");
	}
	else {
		BfWrite bf = UserMessageToBfWrite(buf);
		bf.WriteByte(author); // Message author
		bf.WriteByte(true); // Chat message
		bf.WriteString(buffer); // Message text
	}

	EndMessage();
}

/**
 * This function should only be used right in front of
 * MC_PrintToChatAll or MC_PrintToChatAllEx. It causes those functions
 * to skip the specified client when printing the message.
 * After printing the message, the client will no longer be skipped.
 * 
 * @param client   Client index
 */
stock void MC_SkipNextClient(int client) {
	if (client <= 0 || client > MaxClients) {
		ThrowError("Invalid client index %i", client);
	}
	
	MC_SkipList[client] = true;
}

/**
 * Checks if the colors trie is initialized and initializes it if it's not (used internally)
 * 
 * @return			No return
 */
stock void MC_CheckTrie() {
	if (MC_Trie == null) {
		MC_Trie = MC_InitColorTrie();
	}
}

stock StringMap MC_GetTrie() {
	MC_CheckTrie();
	return MC_Trie;
}

/**
 * Replaces color tags in a string with color codes (used internally by MC_PrintToChat, MC_PrintToChatAll, MC_PrintToChatEx, and MC_PrintToChatAllEx
 *
 * @param buffer		String.
 * @param author		Optional client index to use for {teamcolor} tags, or 0 for none
 * @param removeTags	Optional boolean value to determine whether we're replacing tags with colors, or just removing tags, used by MC_RemoveTags
 * @param maxlen		Optional value for max buffer length, used by MC_RemoveTags
 * 
 * On error/Errors:		If the client index passed for author is invalid or not in game.
 */
stock void MC_ReplaceColorCodes(char[] buffer, int author = 0, bool removeTags = false, int maxlen = MAX_BUFFER_LENGTH) {
	MC_CheckTrie();
	if (removeTags) {
		ReplaceString(buffer, maxlen, "{default}", "", false);
		ReplaceString(buffer, maxlen, "{teamcolor}", "", false);
	}

	if (author != 0 && !removeTags) {
		if (author < 0 || author > MaxClients) {
			ThrowError("Invalid client index %i", author);
		}

		if (!IsClientInGame(author)) {
			ThrowError("Client %i is not in game", author);
		}

		ReplaceString(buffer, maxlen, "{teamcolor}", "\x03", false);
	}

	if (!IsSource2009())
	{
		ReplaceString(buffer, maxlen, "\n", " ");
	}

	int cursor = 0;
	char value[32];
	char tag[32], buff[32]; 
	char[] output = new char[maxlen];

	strcopy(output, maxlen, buffer);
	// Since the string's size is going to be changing, output will hold the replaced string and we'll search buffer

	Regex regex = new Regex("{[#a-zA-Z0-9]+}");
	for (int i = 0; i < 1000; i++) { // The RegEx extension is quite flaky, so we have to loop here :/. This loop is supposed to be infinite and broken by return, but conditions have been added to be safe.
		if (regex.Match(buffer[cursor]) < 1) {
			delete regex;
			strcopy(buffer, maxlen, output);
			return;
		}

		regex.GetSubString(0, tag, sizeof(tag));
		MC_StrToLower(tag);
		cursor = StrContains(buffer[cursor], tag, false) + cursor + 1;
		strcopy(buff, sizeof(buff), tag);
		ReplaceString(buff, sizeof(buff), "{", "");
		ReplaceString(buff, sizeof(buff), "}", "");

		if (buff[0] == '#') {
			if (strlen(buff) == 7) {
				Format(buff, sizeof(buff), "\x07%s", buff[1]);
			}
			else if (strlen(buff) == 9) {
				Format(buff, sizeof(buff), "\x08%s", buff[1]);
			}
			else {
				continue;
			}

			if (removeTags) {
				ReplaceString(output, maxlen, tag, "", false);
			}
			else {
				ReplaceString(output, maxlen, tag, buff, false);
			}
		}
		else if (!MC_Trie.GetString(buff, value, sizeof(value))) {
			continue;
		}

		if (removeTags) {
			ReplaceString(output, maxlen, tag, "", false);
		}
		else {
			ReplaceString(output, maxlen, tag, value, false);
		}
	}
	LogError("[MORE COLORS] Infinite loop broken.");
}

/**
 * Gets a part of a string
 * 
 * @param input			String to get the part from
 * @param output		Buffer to write to
 * @param maxlen		Max length of output buffer
 * @param start			Position to start at
 * @param numChars		Number of characters to return, or 0 for the end of the string
 */
stock void CSubString(const char[] input, char[] output, int maxlen, int start, int numChars = 0) {
	int i = 0;
	for (;;) {
		if (i == maxlen - 1 || i >= numChars || input[start + i] == '\0') {
			output[i] = '\0';
			return;
		}

		output[i] = input[start + i];
		i++;
	}
}

/**
 * Converts a string to lowercase
 * 
 * @param buffer		String to convert
 */
stock void MC_StrToLower(char[] buffer) {
	int len = strlen(buffer);
	for (int i = 0; i < len; i++) {
		buffer[i] = CharToLower(buffer[i]);
	}
}

/**
 * Adds a color to the colors trie
 *
 * @param name			Color name, without braces
 * @param color			Hexadecimal representation of the color (0xRRGGBB)
 * @return				True if color was added successfully, false if a color already exists with that name
 */
stock bool MC_AddColor(const char[] name, int color) {
	MC_CheckTrie();

	char value[32];

	if (MC_Trie.GetString(name, value, sizeof(value))) {
		return false;
	}

	char newName[64];
	strcopy(newName, sizeof(newName), name);

	MC_StrToLower(newName);
	MC_Trie.SetValue(newName, color);
	return true;
}

/**
 * Removes color tags from a message
 * 
 * @param message		Message to remove tags from
 * @param maxlen		Maximum buffer length
 */
stock void MC_RemoveTags(char[] message, int maxlen) {
	MC_ReplaceColorCodes(message, 0, true, maxlen);
}

/**
 * Replies to a command with colors
 * 
 * @param client		Client to reply to
 * @param message		Message (formatting rules)
 */
stock void MC_ReplyToCommand(int client, const char[] message, any ...) {
	char buffer[MAX_BUFFER_LENGTH];
	SetGlobalTransTarget(client);
	VFormat(buffer, sizeof(buffer), message, 3);

	if (client == 0) {
		MC_RemoveTags(buffer, sizeof(buffer));
		PrintToServer("%s", buffer);
	}

	if (GetCmdReplySource() == SM_REPLY_TO_CONSOLE) {
		MC_RemoveTags(buffer, sizeof(buffer));
		PrintToConsole(client, "%s", buffer);
	}
	else {
		MC_PrintToChat(client, "%s", buffer);
	}
}

/**
 * Replies to a command with colors
 * 
 * @param client		Client to reply to
 * @param author		Client to use for {teamcolor}
 * @param message		Message (formatting rules)
 */
stock void MC_ReplyToCommandEx(int client, int author, const char[] message, any ...) {
	char buffer[MAX_BUFFER_LENGTH];
	SetGlobalTransTarget(client);
	VFormat(buffer, sizeof(buffer), message, 4);

	if (client == 0) {
		MC_RemoveTags(buffer, sizeof(buffer));
		PrintToServer("%s", buffer);
	}

	if (GetCmdReplySource() == SM_REPLY_TO_CONSOLE) {
		MC_RemoveTags(buffer, sizeof(buffer));
		PrintToConsole(client, "%s", buffer);
	}
	else {
		MC_PrintToChatEx(client, author, "%s", buffer);
	}
}

/**
 * Displays usage of an admin command to users depending on the 
 * setting of the sm_show_activity cvar.  
 *
 * This version does not display a message to the originating client 
 * if used from chat triggers or menus.  If manual replies are used 
 * for these cases, then this function will suffice.  Otherwise, 
 * MC_ShowActivity2() is slightly more useful.
 * Supports color tags.
 *
 * @param client		Client index doing the action, or 0 for server.
 * @param format		Formatting rules.
 * @param ...			Variable number of format parameters.
 * @error
 */
stock int MC_ShowActivity(int client, const char[] format, any ...) {
	if (sm_show_activity == null) {
		sm_show_activity = FindConVar("sm_show_activity");
	}

	char tag[] = "[SM] ";

	char szBuffer[MC_MAX_MESSAGE_LENGTH];
	//char szCMessage[MC_MAX_MESSAGE_LENGTH];
	int value = sm_show_activity.IntValue;
	ReplySource replyto = GetCmdReplySource();

	char name[MAX_NAME_LENGTH] = "Console";
	char sign[MAX_NAME_LENGTH] = "ADMIN";
	bool display_in_chat = false;
	if (client != 0) {
		if (client < 0 || client > MaxClients || !IsClientConnected(client))
			ThrowError("Client index %d is invalid", client);

		GetClientName(client, name, sizeof(name));
		AdminId id = GetUserAdmin(client);
		if (id == INVALID_ADMIN_ID || !id.HasFlag(Admin_Generic, Access_Effective)) {
			sign = "PLAYER";
		}

		/* Display the message to the client? */
		if (replyto == SM_REPLY_TO_CONSOLE) {
			SetGlobalTransTarget(client);
			VFormat(szBuffer, sizeof(szBuffer), format, 3);

			MC_RemoveTags(szBuffer, sizeof(szBuffer));
			PrintToConsole(client, "%s%s\n", tag, szBuffer);
			display_in_chat = true;
		}
	}
	else {
		SetGlobalTransTarget(LANG_SERVER);
		VFormat(szBuffer, sizeof(szBuffer), format, 3);

		MC_RemoveTags(szBuffer, sizeof(szBuffer));
		PrintToServer("%s%s\n", tag, szBuffer);
	}

	if (!value) {
		return 1;
	}

	for (int i = 1; i <= MaxClients; ++i) {
		if (!IsClientInGame(i) || IsFakeClient(i) || (display_in_chat && i == client)) {
			continue;
		}

		AdminId id = GetUserAdmin(i);
		SetGlobalTransTarget(i);
		if (id == INVALID_ADMIN_ID || !id.HasFlag(Admin_Generic, Access_Effective)) {
			/* Treat this as a normal user. */
			if ((value & 1) | (value & 2)) {
				char newsign[MAX_NAME_LENGTH];

				if ((value & 2) || (i == client)) {
					newsign = name;
				}
				else {
					newsign = sign;
				}

				VFormat(szBuffer, sizeof(szBuffer), format, 3);

				MC_PrintToChatEx(i, client, "%s%s: %s", tag, newsign, szBuffer);
			}
		}
		else {
			/* Treat this as an admin user */
			bool is_root = id.HasFlag(Admin_Root, Access_Effective);
			if ((value & 4) || (value & 8) || ((value & 16) && is_root)) {
				char newsign[MAX_NAME_LENGTH];

				if ((value & 8) || ((value & 16) && is_root) || (i == client)) {
					newsign = name;
				}
				else {
					newsign = sign;
				}

				VFormat(szBuffer, sizeof(szBuffer), format, 3);

				MC_PrintToChatEx(i, client, "%s%s: %s", tag, newsign, szBuffer);
			}
		}
	}

	return 1;
}

/**
 * Same as MC_ShowActivity(), except the tag parameter is used instead of "[SM] " (note that you must supply any spacing).
 * Supports color tags.
 *
 * @param client		Client index doing the action, or 0 for server.
 * @param tags			Tag to display with.
 * @param format		Formatting rules.
 * @param ...			Variable number of format parameters.
 * @error
 */
stock int MC_ShowActivityEx(int client, const char[] tag, const char[] format, any ...) {
	if (sm_show_activity == null) {
		sm_show_activity = FindConVar("sm_show_activity");
	}

	char szBuffer[MC_MAX_MESSAGE_LENGTH];
	//char szCMessage[MC_MAX_MESSAGE_LENGTH];
	int value = sm_show_activity.IntValue;
	ReplySource replyto = GetCmdReplySource();

	char name[MAX_NAME_LENGTH] = "Console";
	char sign[MAX_NAME_LENGTH] = "ADMIN";
	bool display_in_chat = false;
	if (client != 0) {
		if (client < 0 || client > MaxClients || !IsClientConnected(client)) {
			ThrowError("Client index %d is invalid", client);
		}

		GetClientName(client, name, sizeof(name));
		AdminId id = GetUserAdmin(client);
		if (id == INVALID_ADMIN_ID || !id.HasFlag(Admin_Generic, Access_Effective)) {
			sign = "PLAYER";
		}

		/* Display the message to the client? */
		if (replyto == SM_REPLY_TO_CONSOLE) {
			SetGlobalTransTarget(client);
			VFormat(szBuffer, sizeof(szBuffer), format, 4);

			MC_RemoveTags(szBuffer, sizeof(szBuffer));
			PrintToConsole(client, "%s%s\n", tag, szBuffer);
			display_in_chat = true;
		}
	}
	else {
		SetGlobalTransTarget(LANG_SERVER);
		VFormat(szBuffer, sizeof(szBuffer), format, 4);

		MC_RemoveTags(szBuffer, sizeof(szBuffer));
		PrintToServer("%s%s\n", tag, szBuffer);
	}

	if (!value) {
		return 1;
	}

	for (int i = 1; i <= MaxClients; ++i) {
		if (!IsClientInGame(i) || IsFakeClient(i) || (display_in_chat && i == client)) {
			continue;
		}

		AdminId id = GetUserAdmin(i);
		SetGlobalTransTarget(i);
		if (id == INVALID_ADMIN_ID || !id.HasFlag(Admin_Generic, Access_Effective)) {
			/* Treat this as a normal user. */
			if ((value & 1) | (value & 2)) {
				char newsign[MAX_NAME_LENGTH];

				if ((value & 2) || (i == client)) {
					newsign = name;
				}
				else {
					newsign = sign;
				}

				VFormat(szBuffer, sizeof(szBuffer), format, 4);

				MC_PrintToChatEx(i, client, "%s%s: %s", tag, newsign, szBuffer);
			}
		}
		else {
			/* Treat this as an admin user */
			bool is_root = id.HasFlag(Admin_Root, Access_Effective);
			if ((value & 4) || (value & 8) || ((value & 16) && is_root)) {
				char newsign[MAX_NAME_LENGTH];

				if ((value & 8) || ((value & 16) && is_root) || (i == client)) {
					newsign = name;
				}
				else {
					newsign = sign;
				}

				VFormat(szBuffer, sizeof(szBuffer), format, 4);

				MC_PrintToChatEx(i, client, "%s%s: %s", tag, newsign, szBuffer);
			}
		}
	}

	return 1;
}

/**
 * Displays usage of an admin command to users depending on the setting of the sm_show_activity cvar.
 * All users receive a message in their chat text, except for the originating client, 
 * who receives the message based on the current ReplySource.
 * Supports color tags.
 *
 * @param client		Client index doing the action, or 0 for server.
 * @param tags			Tag to prepend to the message.
 * @param format		Formatting rules.
 * @param ...			Variable number of format parameters.
 * @error
 */
stock int MC_ShowActivity2(int client, const char[] tag, const char[] format, any ...) {
	if (sm_show_activity == null) {
		sm_show_activity = FindConVar("sm_show_activity");
	}

	char szBuffer[MC_MAX_MESSAGE_LENGTH];
	//char szCMessage[MC_MAX_MESSAGE_LENGTH];
	int value = sm_show_activity.IntValue;
	// ReplySource replyto = GetCmdReplySource();

	char name[MAX_NAME_LENGTH] = "Console";
	char sign[MAX_NAME_LENGTH] = "ADMIN";

	if (client != 0) {
		if (client < 0 || client > MaxClients || !IsClientConnected(client)) {
			ThrowError("Client index %d is invalid", client);
		}

		GetClientName(client, name, sizeof(name));

		AdminId id = GetUserAdmin(client);
		if (id == INVALID_ADMIN_ID || !id.HasFlag(Admin_Generic, Access_Effective)) {
			sign = "PLAYER";
		}

		SetGlobalTransTarget(client);
		VFormat(szBuffer, sizeof(szBuffer), format, 4);

		/* We don't display directly to the console because the chat text 
		 * simply gets added to the console, so we don't want it to print 
		 * twice.
		 */
		MC_PrintToChatEx(client, client, "%s%s", tag, szBuffer);
	}
	else {
		SetGlobalTransTarget(LANG_SERVER);
		VFormat(szBuffer, sizeof(szBuffer), format, 4);

		MC_RemoveTags(szBuffer, sizeof(szBuffer));
		PrintToServer("%s%s\n", tag, szBuffer);
	}

	if (!value) {
		return 1;
	}

	for (int i = 1; i <= MaxClients; ++i) {
		if (!IsClientInGame(i) || IsFakeClient(i) || i == client) {
			continue;
		}

		AdminId id = GetUserAdmin(i);
		SetGlobalTransTarget(i);
		if (id == INVALID_ADMIN_ID || !id.HasFlag(Admin_Generic, Access_Effective)) {
			/* Treat this as a normal user. */
			if ((value & 1) | (value & 2)) {
				char newsign[MAX_NAME_LENGTH];

				if ((value & 2)) {
					newsign = name;
				}
				else {
					newsign = sign;
				}

				VFormat(szBuffer, sizeof(szBuffer), format, 4);

				MC_PrintToChatEx(i, client, "%s%s: %s", tag, newsign, szBuffer);
			}
		}
		else {
			/* Treat this as an admin user */
			bool is_root = id.HasFlag(Admin_Root, Access_Effective);
			if ((value & 4) || (value & 8) || ((value & 16) && is_root)) {
				char newsign[MAX_NAME_LENGTH];
				

				if ((value & 8) || ((value & 16) && is_root)) {
					newsign = name;
				}
				else {
					newsign = sign;
				}

				VFormat(szBuffer, sizeof(szBuffer), format, 4);

				MC_PrintToChatEx(i, client, "%s%s: %s", tag, newsign, szBuffer);
			}
		}
	}

	return 1;
}

/**
 * Determines whether a color name exists
 * 
 * @param color			The color name to check
 * @return				True if the color exists, false otherwise
 */
stock bool CColorExists(const char[] color) {
	MC_CheckTrie();
	char temp[32];
	return MC_Trie.GetString(color, temp, sizeof(temp));
}

/**
 * Returns the hexadecimal representation of a client's team color (will NOT initialize the trie)
 *
 * @param client		Client to get the team color for
 * @param value			Client's team color, or green if unknown
 * @param size			Size of the value parameter
 * @return				Client's team color has been found
 * On error/Errors:		If the client index passed is invalid or not in game.
 */
stock bool CGetTeamColor(int client, char[] value, int size) {
	if (client <= 0 || client > MaxClients) {
		ThrowError("Invalid client index %i", client);
		return false;
	}

	if (!IsClientInGame(client)) {
		ThrowError("Client %i is not in game", client);
		return false;
	}

	StringMap smTrie = MC_GetTrie();
	if (smTrie == null)
	{
		ThrowError("No color in StringMap tree", client);
		return false;
	}

	switch(GetClientTeam(client)) {
		case 1: {
			smTrie.GetString("gray", value, size);
		}
		case 2: {
			smTrie.GetString("red", value, size);
		}
		case 3: {
			smTrie.GetString("blue", value, size);
		}
		default: {
			smTrie.GetString("green", value, size);
		}
	}

	return true;
}

stock StringMap MC_InitColorTrie() {
	StringMap hTrie = new StringMap();

	if (IsSource2009()) {
		SetTrieString(hTrie, "default", "\x01");
		SetTrieString(hTrie, "teamcolor", "\x03");

		SetTrieString(hTrie, "aliceblue", "\x07F0F8FF");
		SetTrieString(hTrie, "allies", "\x074D7942"); // same as Allies team in DoD:S
		SetTrieString(hTrie, "ancient", "\x07EB4B4B"); // same as Ancient item rarity in Dota 2
		SetTrieString(hTrie, "antiquewhite", "\x07FAEBD7");
		SetTrieString(hTrie, "aqua", "\x0700FFFF");
		SetTrieString(hTrie, "aquamarine", "\x077FFFD4");
		SetTrieString(hTrie, "arcana", "\x07ADE55C"); // same as Arcana item rarity in Dota 2
		SetTrieString(hTrie, "axis", "\x07FF4040"); // same as Axis team in DoD:S
		SetTrieString(hTrie, "azure", "\x07007FFF");
		SetTrieString(hTrie, "beige", "\x07F5F5DC");
		SetTrieString(hTrie, "bisque", "\x07FFE4C4");
		SetTrieString(hTrie, "black", "\x07000000");
		SetTrieString(hTrie, "blanchedalmond", "\x07FFEBCD");
		SetTrieString(hTrie, "blue", "\x0799CCFF"); // same as BLU/Counter-Terrorist team color
		SetTrieString(hTrie, "blueviolet", "\x078A2BE2");
		SetTrieString(hTrie, "brown", "\x07A52A2A");
		SetTrieString(hTrie, "burlywood", "\x07DEB887");
		SetTrieString(hTrie, "cadetblue", "\x075F9EA0");
		SetTrieString(hTrie, "chartreuse", "\x077FFF00");
		SetTrieString(hTrie, "chocolate", "\x07D2691E");
		SetTrieString(hTrie, "collectors", "\x07AA0000"); // same as Collector's item quality in TF2
		SetTrieString(hTrie, "common", "\x07B0C3D9"); // same as Common item rarity in Dota 2
		SetTrieString(hTrie, "community", "\x0770B04A"); // same as Community item quality in TF2
		SetTrieString(hTrie, "coral", "\x07FF7F50");
		SetTrieString(hTrie, "cornflowerblue", "\x076495ED");
		SetTrieString(hTrie, "cornsilk", "\x07FFF8DC");
		SetTrieString(hTrie, "corrupted", "\x07A32C2E"); // same as Corrupted item quality in Dota 2
		SetTrieString(hTrie, "crimson", "\x07DC143C");
		SetTrieString(hTrie, "cyan", "\x0700FFFF");
		SetTrieString(hTrie, "darkblue", "\x0700008B");
		SetTrieString(hTrie, "darkcyan", "\x07008B8B");
		SetTrieString(hTrie, "darkgoldenrod", "\x07B8860B");
		SetTrieString(hTrie, "darkgray", "\x07A9A9A9");
		SetTrieString(hTrie, "darkgrey", "\x07A9A9A9");
		SetTrieString(hTrie, "darkgreen", "\x07006400");
		SetTrieString(hTrie, "darkkhaki", "\x07BDB76B");
		SetTrieString(hTrie, "darkmagenta", "\x078B008B");
		SetTrieString(hTrie, "darkolivegreen", "\x07556B2F");
		SetTrieString(hTrie, "darkorange", "\x07FF8C00");
		SetTrieString(hTrie, "darkorchid", "\x079932CC");
		SetTrieString(hTrie, "darkred", "\x078B0000");
		SetTrieString(hTrie, "darksalmon", "\x07E9967A");
		SetTrieString(hTrie, "darkseagreen", "\x078FBC8F");
		SetTrieString(hTrie, "darkslateblue", "\x07483D8B");
		SetTrieString(hTrie, "darkslategray", "\x072F4F4F");
		SetTrieString(hTrie, "darkslategrey", "\x072F4F4F");
		SetTrieString(hTrie, "darkturquoise", "\x0700CED1");
		SetTrieString(hTrie, "darkviolet", "\x079400D3");
		SetTrieString(hTrie, "deeppink", "\x07FF1493");
		SetTrieString(hTrie, "deepskyblue", "\x0700BFFF");
		SetTrieString(hTrie, "dimgray", "\x07696969");
		SetTrieString(hTrie, "dimgrey", "\x07696969");
		SetTrieString(hTrie, "dodgerblue", "\x071E90FF");
		SetTrieString(hTrie, "exalted", "\x07CCCCCD"); // same as Exalted item quality in Dota 2
		SetTrieString(hTrie, "firebrick", "\x07B22222");
		SetTrieString(hTrie, "floralwhite", "\x07FFFAF0");
		SetTrieString(hTrie, "forestgreen", "\x07228B22");
		SetTrieString(hTrie, "frozen", "\x074983B3"); // same as Frozen item quality in Dota 2
		SetTrieString(hTrie, "fuchsia", "\x07FF00FF");
		SetTrieString(hTrie, "fullblue", "\x070000FF");
		SetTrieString(hTrie, "fullred", "\x07FF0000");
		SetTrieString(hTrie, "gainsboro", "\x07DCDCDC");
		SetTrieString(hTrie, "genuine", "\x074D7455"); // same as Genuine item quality in TF2
		SetTrieString(hTrie, "ghostwhite", "\x07F8F8FF");
		SetTrieString(hTrie, "gold", "\x07FFD700");
		SetTrieString(hTrie, "goldenrod", "\x07DAA520");
		SetTrieString(hTrie, "gray", "\x07CCCCCC"); // same as spectator team color
		SetTrieString(hTrie, "grey", "\x07CCCCCC");
		SetTrieString(hTrie, "green", "\x073EFF3E");
		SetTrieString(hTrie, "greenyellow", "\x07ADFF2F");
		SetTrieString(hTrie, "haunted", "\x0738F3AB"); // same as Haunted item quality in TF2
		SetTrieString(hTrie, "honeydew", "\x07F0FFF0");
		SetTrieString(hTrie, "hotpink", "\x07FF69B4");
		SetTrieString(hTrie, "immortal", "\x07E4AE33"); // same as Immortal item rarity in Dota 2
		SetTrieString(hTrie, "indianred", "\x07CD5C5C");
		SetTrieString(hTrie, "indigo", "\x074B0082");
		SetTrieString(hTrie, "ivory", "\x07FFFFF0");
		SetTrieString(hTrie, "khaki", "\x07F0E68C");
		SetTrieString(hTrie, "lavender", "\x07E6E6FA");
		SetTrieString(hTrie, "lavenderblush", "\x07FFF0F5");
		SetTrieString(hTrie, "lawngreen", "\x077CFC00");
		SetTrieString(hTrie, "legendary", "\x07D32CE6"); // same as Legendary item rarity in Dota 2
		SetTrieString(hTrie, "lemonchiffon", "\x07FFFACD");
		SetTrieString(hTrie, "lightblue", "\x07ADD8E6");
		SetTrieString(hTrie, "lightcoral", "\x07F08080");
		SetTrieString(hTrie, "lightcyan", "\x07E0FFFF");
		SetTrieString(hTrie, "lightgoldenrodyellow", "\x07FAFAD2");
		SetTrieString(hTrie, "lightgray", "\x07D3D3D3");
		SetTrieString(hTrie, "lightgrey", "\x07D3D3D3");
		SetTrieString(hTrie, "lightgreen", "\x0799FF99");
		SetTrieString(hTrie, "lightpink", "\x07FFB6C1");
		SetTrieString(hTrie, "lightsalmon", "\x07FFA07A");
		SetTrieString(hTrie, "lightseagreen", "\x0720B2AA");
		SetTrieString(hTrie, "lightskyblue", "\x0787CEFA");
		SetTrieString(hTrie, "lightslategray", "\x07778899");
		SetTrieString(hTrie, "lightslategrey", "\x07778899");
		SetTrieString(hTrie, "lightsteelblue", "\x07B0C4DE");
		SetTrieString(hTrie, "lightyellow", "\x07FFFFE0");
		SetTrieString(hTrie, "lime", "\x0700FF00");
		SetTrieString(hTrie, "limegreen", "\x0732CD32");
		SetTrieString(hTrie, "linen", "\x07FAF0E6");
		SetTrieString(hTrie, "magenta", "\x07FF00FF");
		SetTrieString(hTrie, "maroon", "\x07800000");
		SetTrieString(hTrie, "mediumaquamarine", "\x0766CDAA");
		SetTrieString(hTrie, "mediumblue", "\x070000CD");
		SetTrieString(hTrie, "mediumorchid", "\x07BA55D3");
		SetTrieString(hTrie, "mediumpurple", "\x079370D8");
		SetTrieString(hTrie, "mediumseagreen", "\x073CB371");
		SetTrieString(hTrie, "mediumslateblue", "\x077B68EE");
		SetTrieString(hTrie, "mediumspringgreen", "\x0700FA9A");
		SetTrieString(hTrie, "mediumturquoise", "\x0748D1CC");
		SetTrieString(hTrie, "mediumvioletred", "\x07C71585");
		SetTrieString(hTrie, "midnightblue", "\x07191970");
		SetTrieString(hTrie, "mintcream", "\x07F5FFFA");
		SetTrieString(hTrie, "mistyrose", "\x07FFE4E1");
		SetTrieString(hTrie, "moccasin", "\x07FFE4B5");
		SetTrieString(hTrie, "mythical", "\x078847FF"); // same as Mythical item rarity in Dota 2
		SetTrieString(hTrie, "navajowhite", "\x07FFDEAD");
		SetTrieString(hTrie, "navy", "\x07000080");
		SetTrieString(hTrie, "normal", "\x07B2B2B2"); // same as Normal item quality in TF2
		SetTrieString(hTrie, "oldlace", "\x07FDF5E6");
		SetTrieString(hTrie, "olive", "\x079EC34F");
		SetTrieString(hTrie, "olivedrab", "\x076B8E23");
		SetTrieString(hTrie, "orange", "\x07FFA500");
		SetTrieString(hTrie, "orangered", "\x07FF4500");
		SetTrieString(hTrie, "orchid", "\x07DA70D6");
		SetTrieString(hTrie, "palegoldenrod", "\x07EEE8AA");
		SetTrieString(hTrie, "palegreen", "\x0798FB98");
		SetTrieString(hTrie, "paleturquoise", "\x07AFEEEE");
		SetTrieString(hTrie, "palevioletred", "\x07D87093");
		SetTrieString(hTrie, "papayawhip", "\x07FFEFD5");
		SetTrieString(hTrie, "peachpuff", "\x07FFDAB9");
		SetTrieString(hTrie, "peru", "\x07CD853F");
		SetTrieString(hTrie, "pink", "\x07FFC0CB");
		SetTrieString(hTrie, "plum", "\x07DDA0DD");
		SetTrieString(hTrie, "powderblue", "\x07B0E0E6");
		SetTrieString(hTrie, "purple", "\x07800080");
		SetTrieString(hTrie, "rare", "\x074B69FF"); // same as Rare item rarity in Dota 2
		SetTrieString(hTrie, "red", "\x07FF4040"); // same as RED/Terrorist team color
		SetTrieString(hTrie, "rosybrown", "\x07BC8F8F");
		SetTrieString(hTrie, "royalblue", "\x074169E1");
		SetTrieString(hTrie, "saddlebrown", "\x078B4513");
		SetTrieString(hTrie, "salmon", "\x07FA8072");
		SetTrieString(hTrie, "sandybrown", "\x07F4A460");
		SetTrieString(hTrie, "seagreen", "\x072E8B57");
		SetTrieString(hTrie, "seashell", "\x07FFF5EE");
		SetTrieString(hTrie, "selfmade", "\x0770B04A"); // same as Self-Made item quality in TF2
		SetTrieString(hTrie, "sienna", "\x07A0522D");
		SetTrieString(hTrie, "silver", "\x07C0C0C0");
		SetTrieString(hTrie, "skyblue", "\x0787CEEB");
		SetTrieString(hTrie, "slateblue", "\x076A5ACD");
		SetTrieString(hTrie, "slategray", "\x07708090");
		SetTrieString(hTrie, "slategrey", "\x07708090");
		SetTrieString(hTrie, "snow", "\x07FFFAFA");
		SetTrieString(hTrie, "springgreen", "\x0700FF7F");
		SetTrieString(hTrie, "steelblue", "\x074682B4");
		SetTrieString(hTrie, "strange", "\x07CF6A32"); // same as Strange item quality in TF2
		SetTrieString(hTrie, "tan", "\x07D2B48C");
		SetTrieString(hTrie, "teal", "\x07008080");
		SetTrieString(hTrie, "thistle", "\x07D8BFD8");
		SetTrieString(hTrie, "tomato", "\x07FF6347");
		SetTrieString(hTrie, "turquoise", "\x0740E0D0");
		SetTrieString(hTrie, "uncommon", "\x07B0C3D9"); // same as Uncommon item rarity in Dota 2
		SetTrieString(hTrie, "unique", "\x07FFD700"); // same as Unique item quality in TF2
		SetTrieString(hTrie, "unusual", "\x078650AC"); // same as Unusual item quality in TF2
		SetTrieString(hTrie, "valve", "\x07A50F79"); // same as Valve item quality in TF2
		SetTrieString(hTrie, "vintage", "\x07476291"); // same as Vintage item quality in TF2
		SetTrieString(hTrie, "violet", "\x07EE82EE");
		SetTrieString(hTrie, "wheat", "\x07F5DEB3");
		SetTrieString(hTrie, "white", "\x07FFFFFF");
		SetTrieString(hTrie, "whitesmoke", "\x07F5F5F5");
		SetTrieString(hTrie, "yellow", "\x07FFFF00");
		SetTrieString(hTrie, "yellowgreen", "\x079ACD32");
	} else {
		SetTrieString(hTrie, "default", "\x01");       // "\x01" "{default}"
		SetTrieString(hTrie, "teamcolor", "\x03");     // "\x03" "{lightgreen}" "\x03" "{orange}" "\x03" "{blue}" "\x03" "{purple}"

		SetTrieString(hTrie, "red", "\x07");           // "\x07" "{red}"
		SetTrieString(hTrie, "lightred", "\x0F");      // "\x0F" "{lightred}"
		SetTrieString(hTrie, "darkred", "\x02");       // "\x02" "{darkred}"
		SetTrieString(hTrie, "bluegrey", "\x0A");      // "\x0A" "{lightblue}"
		SetTrieString(hTrie, "blue", "\x0B");          // "\x0B" "{steelblue}"
		SetTrieString(hTrie, "darkblue", "\x0C");      // "\x0C" "{darkblue}"
		SetTrieString(hTrie, "purple", "\x03");
		SetTrieString(hTrie, "orchid", "\x0E");        // "\x0E" "{pink}"
		SetTrieString(hTrie, "yellow", "\x09");        // "\x09" "{yellow}"
		SetTrieString(hTrie, "gold", "\x10");
		SetTrieString(hTrie, "lightgreen", "\x05");    // "\x05" "{olive}"
		SetTrieString(hTrie, "green", "\x04");         // "\x04" "{green}"
		SetTrieString(hTrie, "lime", "\x06");          // "\x06" "{lime}"
		SetTrieString(hTrie, "grey", "\x08");          // "\x08" "{grey}"
		SetTrieString(hTrie, "grey2", "\x0D");

		// Additional color names for ckSurf backwards compatibility
		SetTrieString(hTrie, "bluegray", "\x0A"); // using bluegrey
		SetTrieString(hTrie, "gray", "\x08"); // using gray
		SetTrieString(hTrie, "gray2", "\x0D"); // using gray2
		SetTrieString(hTrie, "orange", "\x10"); // using gold
		SetTrieString(hTrie, "steelblue", "\x0D"); // using grey2
		SetTrieString(hTrie, "pink", "\x0E"); // using orchid
		SetTrieString(hTrie, "lightblue", "\x0A"); // using bluegrey
		SetTrieString(hTrie, "olive", "\x05"); // using lightgreen
	}

	// SetTrieString(hTrie, "engine 1", "\x01");
	// SetTrieString(hTrie, "engine 2", "\x02");
	// SetTrieString(hTrie, "engine 3", "\x03");
	// SetTrieString(hTrie, "engine 4", "\x04");
	// SetTrieString(hTrie, "engine 5", "\x05");
	// SetTrieString(hTrie, "engine 6", "\x06");
	// SetTrieString(hTrie, "engine 7", "\x07");
	// SetTrieString(hTrie, "engine 8", "\x08");
	// SetTrieString(hTrie, "engine 9", "\x09");
	// SetTrieString(hTrie, "engine 10", "\x0A");
	// SetTrieString(hTrie, "engine 11", "\x0B");
	// SetTrieString(hTrie, "engine 12", "\x0C");
	// SetTrieString(hTrie, "engine 13", "\x0D");
	// SetTrieString(hTrie, "engine 14", "\x0E");
	// SetTrieString(hTrie, "engine 15", "\x0F");
	// SetTrieString(hTrie, "engine 16", "\x10");

	return hTrie;
}
